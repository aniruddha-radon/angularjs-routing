app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
    .when("/", {
        templateUrl: "../html/main.html"
    })
    .when("/about", {
        templateUrl: "../html/about.html"
    })
    .when("/contact", {
        templateUrl: "../html/contact.html"
    });

    // $locationProvider.html5Mode({
    //     enabled: true,
    //     requireBase: false
    // });
})